package com.soft.pezo.peoplecard.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.soft.pezo.peoplecard.PersonAdapter;
import com.soft.pezo.peoplecard.R;
import com.soft.pezo.peoplecard.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;

public class GetHttpClients
        extends AsyncTask<Void, Void, String>{

    private List<Person> httpList;
    private RecyclerView httpRecicler;
    private RecyclerView.Adapter httpAdapter;
    private Context httpContext;
    ProgressDialog progressDialog;

    public GetHttpClients(List<Person> httpList, RecyclerView httpRecicler, RecyclerView.Adapter httpAdapter, Context httpContext) {
        this.httpList = httpList;
        this.httpRecicler = httpRecicler;
        this.httpAdapter = httpAdapter;
        this.httpContext = httpContext;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        progressDialog = ProgressDialog.show(httpContext, httpContext.getString(R.string.popMsgTitulo), httpContext.getString(R.string.popMsgMensaje));
    }

    @Override
    protected String doInBackground(Void... voids) {
        String result = null;

        try{
            String wsURL = "http://demos.hypergis.pe/php/wsCliente.php?num=10&format=json";
            URL url = new URL(wsURL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            result = inputStreamToString(in);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    private String inputStreamToString(InputStream in) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        InputStreamReader isr = new InputStreamReader(in);
        BufferedReader rd = new BufferedReader(isr);
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }

    @Override
    protected void onPostExecute(String s){
        super.onPostExecute(s);
        progressDialog.dismiss();

        try {
            JSONObject jsonObject = new JSONObject(URLDecoder.decode(s, "UTF-8"));
            JSONArray jsonArray = jsonObject.getJSONArray("clientes");
            for(int i=0; i<jsonArray.length(); i++){
                String nombre = jsonArray.getJSONObject(i).getString("cNombre");
                String dni = jsonArray.getJSONObject(i).getString("cDNI");
                int edad = Integer.parseInt(jsonArray.getJSONObject(i).getString("eEdad"));
                String bio = jsonArray.getJSONObject(i).getString("cBiografia");
                String urlFoto = jsonArray.getJSONObject(i).getString("cFotoURL");
                this.httpList.add(new Person(nombre, dni, edad, bio, urlFoto));
            }

            httpAdapter = new PersonAdapter(this.httpList);
            httpRecicler.setAdapter(httpAdapter);

            String msg = String.valueOf( httpAdapter.getItemCount() ) + " registros";
            Toast.makeText(httpContext, msg, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}