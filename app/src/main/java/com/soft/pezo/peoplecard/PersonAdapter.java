package com.soft.pezo.peoplecard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/*
public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder> {
    private List<Person> items;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        public CardView personCardView;
        public ImageView imagen;
        public TextView nombre;
        public TextView dni;
        public TextView edad:

        public PersonViewHolder(View v){
            super(v);
            personCardView = (CardView) v.findViewById(R.id.person_card);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            nombre = (TextView) v.findViewById(R.id.nombre);
            dni = (TextView) v.findViewById(R.id.dni);
            edad = (TextView) v.findViewById(R.id.edad);
        }

        @Override
        public int getItemCount() { return items.size(); }

        Override
        public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        }
    }
}
*/


/**
 * Created by Marlon on 15/08/2018.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder>  {
    private List<Person> items;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public CardView personCardView;
        public ImageView imgFoto;
        public TextView lblNombre;
        public TextView lblDNI;
        public TextView lblEdad;

        public PersonViewHolder(View v) {
            super(v);
            personCardView = (CardView) v.findViewById(R.id.person_card);
            imgFoto = (ImageView) v.findViewById(R.id.imagen);
            lblNombre = (TextView) v.findViewById(R.id.nombre);
            lblDNI = (TextView) v.findViewById(R.id.dni);
            lblEdad = (TextView) v.findViewById(R.id.edad);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public PersonAdapter(List<Person> items) {
        this.items = items;
    }

    public List<Person> getItems(){
        return this.items;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.person_card, viewGroup, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {
        //viewHolder.imgFoto.setImageResource(items.get(i).getImagen());
        viewHolder.lblNombre.setText(items.get(i).getNombre());
        viewHolder.lblDNI.setText("DNI: " + items.get(i).getDni());
        viewHolder.lblEdad.setText("Edad: " + String.valueOf(items.get(i).getEdad()));

        //Implementando evento CLIC en el CardView

        Picasso.with(viewHolder.imgFoto.getContext())   //Contexto
                .load(items.get(i).getUrlFoto())        //URL de foto
                .into(viewHolder.imgFoto);              //Elemento imagen

    }
}

