package com.soft.pezo.peoplecard;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BioActivity extends AppCompatActivity {

    TextView nombre, dni, bio;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nombre = findViewById(R.id.nombre);
        dni = findViewById(R.id.dni);
        bio = findViewById(R.id.biografia);
        imagen = findViewById(R.id.imagen);

        nombre.setText(getIntent().getExtras().getString("curNombre"));
        dni.setText("DNI: " + getIntent().getExtras().getString("curDni"));
        bio.setText(getIntent().getExtras().getString("curBio"));
        imagen.setImageResource(getIntent().getExtras().getInt("curImagen"));
    }

}
