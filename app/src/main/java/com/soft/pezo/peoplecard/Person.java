package com.soft.pezo.peoplecard;

public class Person {

    public Person() {

    }

    public Person(String nombre, String dni, int edad, String bio, String urlfoto) {
        this.nombre = nombre;
        this.dni = dni;
        this.edad = edad;
        this.bio = bio;
        this.urlfoto = urlfoto;
    }

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }


    public String getUrlFoto() { return this.urlfoto; }

    public void setUrlfoto(String foto) { this.urlfoto = foto; }

    private String dni;
    private int edad;
    private String bio;
    private String urlfoto;
}
