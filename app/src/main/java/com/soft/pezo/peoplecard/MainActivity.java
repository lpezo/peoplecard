package com.soft.pezo.peoplecard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.soft.pezo.peoplecard.services.GetHttpClients;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView reciclador;
    private List<Person> items = new ArrayList();
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        reciclador = (RecyclerView) findViewById(R.id.reciclador);
        reciclador.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        reciclador.setLayoutManager(lManager);

        FillPerson();

        //adapter = new PersonAdapter(items);
        //reciclador.setAdapter(adapter);

    }

    private void FillPerson() {
        //items.add(new Person("Juan Perez", "20202020", 30, "Ingeniero de Sistemas", R.drawable.face01, ""));
        //items.add(new Person("Luis Pezo", "07483210", 40, "Ingeniero de Sistemas EPE", R.drawable.face02, ""));
        GetHttpClients ws = new GetHttpClients(items, reciclador, adapter, this);
        ws.execute();
    }
}
